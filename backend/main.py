import shutil
import os

from dotenv import load_dotenv
load_dotenv()

from aws.s3 import S3
from fastapi import FastAPI, UploadFile, File
from typing import List

from models.photo import Photo

app = FastAPI()

s3 = S3()

@app.get('/user')
def get_user():
  user = S3().get_buckets()
  return user


@app.get("/photos")
def get_photos():

  photos = s3.get_files()
  return {
    "photos": photos
  }


@app.post("/photos", status_code=201)
def upload_photo(files: List[UploadFile]=File(...)):

  for image in files:
    director = os.path.join('imagens', image.filename)

    with open(director, "wb") as buffer:
      shutil.copyfileobj(image.file, buffer)
      buffer.close()

    result = s3.upload_file(director, image.filename)
    if not result:
      return {
        "message": "Fail to save image"
      }

  return {
    "messge": "Good!"
  }