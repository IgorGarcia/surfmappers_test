import os
import boto3

service = 's3'
region = os.getenv('aws_region')
aws_id = os.getenv('aws_access_key_id')
aws_access_key=os.getenv('aws_secret_access_key')
bucket_name = os.getenv('aws_bucket_name')

class S3:

  def __init__(self):
    self.s3 = boto3.resource(
      service_name=service,
      region_name=region,
      aws_access_key_id=aws_id,
      aws_secret_access_key=aws_access_key
    )

  def get_files(self):
    objects = []
    for obj in self.s3.Bucket(bucket_name).objects.all():
      objects.append(dir(obj))

    return objects

  def upload_file(self, path_to_file, name_to_file):
    try:
      self.s3.Bucket(bucket_name).upload_file(
        Filename=path_to_file,
        Key=name_to_file
      )
      return True
    except Exception as error:
      print('Error:', error)
      return False