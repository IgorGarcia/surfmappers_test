from typing import Optional
from pydantic import BaseModel

class Photo(BaseModel):
  name: str
  file: str
  approved: Optional[bool] = False